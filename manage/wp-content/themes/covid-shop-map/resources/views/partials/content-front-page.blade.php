@php the_content() @endphp

<div class="card-deck text-center mt-5">
  <div class="card mb-6 box-shadow">
    <div class="card-header">
      <h4 class="my-0">{{ __('New users', 'sage') }}</h4>
    </div>
    <div class="card-body">
      <a href="{{ wp_registration_url() }}" class="btn btn-lg btn-block btn-primary">{{ __('Register', 'sage') }}</a>
    </div>
  </div>
  <div class="card mb-6 box-shadow">
    <div class="card-header">
      <h4 class="my-0">{{ __('Existing users', 'sage') }}</h4>
    </div>
    <div class="card-body">
      <a href="{{ wp_login_url() }}" class="btn btn-lg btn-block btn-outline-primary">{{ __('Login', 'sage') }}</a>
    </div>
  </div>
</div>

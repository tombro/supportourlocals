<nav class="navbar bg-primary-light mb-5 navbar-expand-lg">
  <a class="navbar-brand mr-auto" href="{{ str_replace( '/manage', '', home_url('/') ) }}"><strong>{{ get_bloginfo('name', 'display') }}</strong></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
    @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav', 'walker' => new \App\wp_bootstrap4_navwalker()]) !!}
    @endif
  </div>
</nav>

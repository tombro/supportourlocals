<div class="jumbotron">
  <h1>{!! App::title() !!}</h1>
  <p class="lead">
    {!! sprintf( __('Welcome shopkeepers, <a href="%s">log in</a> to modify your shop, or <a href="%s">register</a> and add your shop.', 'sage'), wp_login_url(), wp_registration_url() ) !!}</p>
</div>

��          �   %   �      P     Q     g     }     �  	   �  -   �     �     �     �     
       	        '  	   =     G     N     a     j     ~  ;   �     �  q   �  %   `  -   �  C   �  l  �     e     |     �     �  	   �  9   �     �          !     @  	   R     \     n     �     �     �     �     �     �  :   �  )   &  }   P  #   �  +   �  I   	                                                                   	             
                                             &larr; Older comments Autoloader not found. By Comments are closed. Continued Error locating <code>%s</code> for inclusion. Existing users Invalid PHP version Invalid WordPress version Latest Posts Login New users Newer comments &rarr; Not Found Pages: Primary Navigation Register Sage &rsaquo; Error Search Results for %s Sorry, but the page you were trying to view does not exist. Sorry, no results were found. Welcome shopkeepers, <a href="%s">log in</a> to modify your shop, or <a href="%s">register</a> and add your shop. You must be using PHP 7.1 or greater. You must be using WordPress 4.7.0 or greater. You must run <code>composer install</code> from the Sage directory. Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-03-19 14:05+0100
PO-Revision-Date: 2020-03-19 14:06+0100
Last-Translator: Tom Broucke <tom@tombrouckeBe>
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
Plural-Forms: nplurals=2; plural=(n != 1);
 &larr; Oudere reacties Autoloader is niet gevonden. Door Reacties zijn gesloten. Lees meer Fout bij het lokaliseren van <code>%s</code> voor opname. Bestaande klanten Ongeldige PHP versie Ongeldige versie van WordPress Laatste Berichten Aanmelden Nieuwe gebruikers Nieuwere reacties &rarr; Niets gevonden Pagina’s: Primaire navigatie Registreren Sage &rsaquo; Fout Zoekresultaten voor %s Sorry, de pagina die je probeert te bezoeken bestaat niet. Helaas, er zijn geen resultaten gevonden. Welkom handelaars, <a href="%s">log in</a> om je shop aan te passen, of <a href="%s">registreer</a> om je shop toe te voegen. Je moet PHP 7.1 of hoger gebruiken. Je moet WordPress 4.7.0 of hoger gebruiken. Je moet <code>composer install</code> uitvoeren vanuit de Sage directory. 
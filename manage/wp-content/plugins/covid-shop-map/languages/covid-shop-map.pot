#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Covid Shop Map\n"
"POT-Creation-Date: 2020-03-20 17:22+0100\n"
"PO-Revision-Date: 2020-03-19 09:31+0100\n"
"Last-Translator: Tom Broucke <tom@tombrouckeBe>\n"
"Language-Team: Tom Broucke <tom@tombrouckeBe>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: covid-shop-map.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: covid-shop-map.php:60
msgid "Shopkeeper"
msgstr ""

#: covid-shop-map.php:61
msgid "Manager"
msgstr ""

#: includes/class-admin.php:51
msgid "Dashboard"
msgstr ""

#: includes/class-cpts.php:17 includes/class-report.php:38
#: includes/class-report.php:39 includes/class-report.php:135
msgid "Shop"
msgstr ""

#: includes/class-cpts.php:18
msgid "Shops"
msgstr ""

#: includes/class-cpts.php:43
msgid "Service"
msgstr ""

#: includes/class-cpts.php:44
msgid "Services"
msgstr ""

#: includes/class-cpts.php:58
msgid "Category"
msgstr ""

#: includes/class-cpts.php:59
msgid "Categories"
msgstr ""

#: includes/class-cpts.php:75 includes/class-report.php:50
#: includes/class-report.php:128
msgid "Report"
msgstr ""

#: includes/class-cpts.php:76 includes/class-report.php:101
msgid "Reports"
msgstr ""

#: includes/class-cpts.php:117
msgid "Add new"
msgstr ""

#: includes/class-cpts.php:118 includes/class-cpts.php:172
#, php-format
msgid "Add new %s"
msgstr ""

#: includes/class-cpts.php:119
#, php-format
msgid "New %s"
msgstr ""

#: includes/class-cpts.php:120 includes/class-cpts.php:170
#, php-format
msgid "Edit %s"
msgstr ""

#: includes/class-cpts.php:121
#, php-format
msgid "View %s"
msgstr ""

#: includes/class-cpts.php:122 includes/class-cpts.php:167
#, php-format
msgid "All %s"
msgstr ""

#: includes/class-cpts.php:123 includes/class-cpts.php:166
#, php-format
msgid "Search %s"
msgstr ""

#: includes/class-cpts.php:124 includes/class-cpts.php:168
#, php-format
msgid "Parent %s"
msgstr ""

#: includes/class-cpts.php:125 includes/class-cpts.php:169
#, php-format
msgid "Parent %s:"
msgstr ""

#: includes/class-cpts.php:126
#, php-format
msgid "No %s found"
msgstr ""

#: includes/class-cpts.php:127
#, php-format
msgid "No %s found in trash"
msgstr ""

#: includes/class-cpts.php:171
#, php-format
msgid "Update %s"
msgstr ""

#: includes/class-cpts.php:173
#, php-format
msgid "New %s name"
msgstr ""

#: includes/class-report.php:22
#, php-format
msgid "Thank you, %s has been reported and will be reviewed shortly."
msgstr ""

#: includes/class-report.php:28 includes/class-report.php:29
#: includes/class-report.php:133
msgid "Name"
msgstr ""

#: includes/class-report.php:32 includes/class-report.php:134
msgid "E-mailaddress"
msgstr ""

#: includes/class-report.php:33
msgid "E-mail"
msgstr ""

#: includes/class-report.php:45 includes/class-report.php:136
msgid "Reason"
msgstr ""

#: includes/class-report.php:54
msgid "No shop was specified."
msgstr ""

#: includes/class-report.php:82
msgid "Shop reported"
msgstr ""

#: includes/class-report.php:83
msgid "Hi"
msgstr ""

#: includes/class-report.php:87
msgid "View report"
msgstr ""

#: includes/class-report.php:88
msgid "Stay safe"
msgstr ""

#: includes/class-report.php:102
msgid "Date"
msgstr ""

#: includes/class-rest-api.php:67
msgid "Filter by postcode"
msgstr ""

#: includes/class-rest-api.php:74
msgid "Filter by city"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Covid Shop Map"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://makeinbelgium.slack.com/"
msgstr ""

#. Description of the plugin/theme
msgid "Show local shops on a map"
msgstr ""

#. Author of the plugin/theme
msgid "Tom Broucke"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://tombroucke.be"
msgstr ""

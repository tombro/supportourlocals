��    +      t  ;   �      �     �  
   �     �  
   �     �     �  	   �                          "     1     D     G     O     T     [     g     s     �  	   �  
   �     �     �     �  	   �     �     �     �     �  
   �          	  	   #  =   -     k  	   w     �     �      �     �  	  �     �     �     �     	  	   	     	  	   -	     7	     =	     D	     P	     \	     o	     �	     �	     �	     �	     �	     �	  !   �	     �	     
     
     ,
     2
  	   >
  	   H
     R
     Y
     b
     g
  
   
     �
  %   �
     �
  N   �
            	   +     5      E     f            *      )          $                                    %                &      (   !               #                                  
   "                           	   '       +                   Add new Add new %s All %s Categories Category Covid Shop Map Dashboard Date E-mail E-mailaddress Edit %s Filter by city Filter by postcode Hi Manager Name New %s New %s name No %s found No %s found in trash No shop was specified. Parent %s Parent %s: Reason Report Reports Search %s Service Services Shop Shop reported Shopkeeper Shops Show local shops on a map Stay safe Thank you, %s has been reported and will be reviewed shortly. Tom Broucke Update %s View %s View report https://makeinbelgium.slack.com/ https://tombroucke.be Project-Id-Version: Covid Shop Map
POT-Creation-Date: 2020-03-20 17:22+0100
PO-Revision-Date: 2020-03-20 17:24+0100
Last-Translator: Tom Broucke <tom@tombrouckeBe>
Language-Team: Tom Broucke <tom@tombrouckeBe>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: covid-shop-map.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Nieuwe toevoegen Nieuw(e) %s toevoegen Alle %s Categorieën Categorie Covid Shop kaart Dashboard Datum E-mail E-mailadres %s bewerken Filter op gemeente Filter op postcode Hallo Manager Naam Nieuw(e) %s Nieuw(e) %s naam Geen %s gevonden Geen %s gevonden in de prullenbak Er werd geen shop gespecifieerd Bovenliggend(e) %s Bovenliggend(e) %s: Reden Rapporteren Aangiftes %s zoeken Dienst Diensten Shop Shop werd gerapporteerd Shopkeeper Shops Lokale winkels op een kaart weergeven Blijf veilig Bedankt, %s werd gerapporteerd en zal binnenkort onder de loep genomen worden. Tom Broucke Bijwerken %s Bekijk %s Bekijk aangifte https://makeinbelgium.slack.com/ https://tombroucke.be 
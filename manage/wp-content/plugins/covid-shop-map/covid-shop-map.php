<?php
/**
 * Plugin Name:     Covid Shop Map
 * Plugin URI:      https://makeinbelgium.slack.com/
 * Description:     Show local shops on a map
 * Author:          Tom Broucke
 * Author URI:      https://tombroucke.be
 * Text Domain:     covid-shop-map
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Covid_Shop_Map
 */

namespace SOL\ShopMap;

if ( ! defined( 'ABSPATH' ) ) exit;

class SOL_Shop_Map {

	private static $instance = null;

	/**
	 * Creates or returns an instance of this class.
	 * @since  1.0.0
	 * @return Plugin_Class_Name A single instance of this class.
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function __construct() {
		$this->includes();
		$this->init();
	}

	private function includes() {
		include 'includes/class-admin.php';
		include 'includes/class-cpts.php';
		include 'includes/class-frontend.php';
		include 'includes/class-report.php';
		include 'includes/class-rest-api.php';
	}

	private function init() {
		$this->load_textdomain();
	}


	public function load_textdomain()
	{
		load_plugin_textdomain( 'covid-shop-map', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
	}

	public static function activate() {
		// Add necessary roles
		add_role( 'shopkeeper', __('Shopkeeper', 'covid-shop-map') );
		add_role( 'manager', __('Manager', 'covid-shop-map') );

		// Remove unused roles
		remove_role( 'subscriber' );
		remove_role( 'editor' );
		remove_role( 'contributor' );
		remove_role( 'author' );

		// Shopkeeper caps
		$shopkeeper_caps = array(
			'read',
			'upload_files',
			'edit_shop', // Remove this role to moderate shops before submitting $shopkeeper->remove_cap( 'edit_shop' );
			'edit_shops',
			'publish_shops',
			'assign_shop_services',
			'assign_shop_category',
			'delete_shop'
		);

		$shopkeeper = get_role('shopkeeper');
		foreach( $shopkeeper_caps as $cap ) {
			$shopkeeper->add_cap( $cap );
		}

		// Manager caps
		$manager_caps = array(
			'read',
			'upload_files',
			'edit_page',
			'edit_pages',
			'read_pages',
			'read_private_pages',
			'edit_published_pages',
			'edit_others_pages',
			'publish_pages',
			'edit_shop',
			'edit_shops',
			'edit_others_shops',
			'publish_shops',
			'read_shop',
			'read_private_shops',
			'delete_shop',
			'delete_shops',
			'manage_shop_services',
			'edit_shop_services',
			'delete_shop_services',
			'assign_shop_services',
			'manage_shop_category',
			'edit_shop_category',
			'delete_shop_category',
			'assign_shop_category',
			'read_report',
			'delete_report'
		);

		$manager = get_role('manager');
		foreach( $manager_caps as $cap ) {
			$manager->add_cap( $cap );
		}

		// Admin caps
		$admin_caps = array(
			'edit_shop',
			'edit_shops',
			'edit_others_shops',
			'publish_shops',
			'read_shop',
			'read_private_shops',
			'delete_shop',
			'delete_shops',
			'manage_shop_services',
			'edit_shop_services',
			'delete_shop_services',
			'assign_shop_services',
			'manage_shop_category',
			'edit_shop_category',
			'delete_shop_category',
			'assign_shop_category',
			'edit_report',
			'edit_reports',
			'edit_others_reports',
			'publish_reports',
			'read_report',
			'read_private_reports',
			'delete_report',
			'delete_reports'
		);

		$administrator = get_role('administrator');
		foreach( $admin_caps as $cap ) {
			$administrator->add_cap( $cap );
		}
	}
}

add_action( 'plugins_loaded', array( 'SOL\ShopMap\\SOL_Shop_Map', 'get_instance' ) );
register_activation_hook( __FILE__, array( 'SOL\ShopMap\\SOL_Shop_Map', 'activate' ) );

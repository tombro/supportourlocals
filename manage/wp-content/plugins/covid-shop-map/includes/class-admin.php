<?php
namespace SOL\ShopMap;

class Admin
{
	function __construct(){
		add_filter( 'pre_get_posts', array( $this, 'list_shops_for_current_shopkeeper' ) );
		add_filter( 'views_edit-shop', array( $this, 'custom_counts' ), 10, 1);
		add_action( 'load-index.php', array( $this, 'dashboard_redirect' ) );
		add_filter( 'login_redirect', array( $this, 'login_redirect' ), 10, 3);
		add_action( 'admin_menu', array( $this, 'clean_up_menu' ) );
		add_action( 'admin_bar_menu', array( $this, 'customize_toolbar' ), 80 );
		add_filter( 'ajax_query_attachments_args', array( $this, 'current_user_attachments' ) );
		add_action( 'template_redirect', array( $this, 'redirect_front_page_logged_in' ));
	}

	public function list_shops_for_current_shopkeeper( $query ) {
		global $pagenow;

		if( 'edit.php' != $pagenow || !$query->is_admin ) {
			return $query;
		}

		if( !current_user_can( 'edit_other_shops' ) ) {
			global $user_ID;
			$query->set('author', $user_ID );
		}
		return $query;
	}

	public function custom_counts($views) {
		if( !current_user_can('edit_other_shops') ) {
			unset($views['all']);
			unset($views['publish']);
			unset($views['pending']);
			unset($views['trash']);
		}
		return $views;
	}

	public function dashboard_redirect(){
		wp_redirect( admin_url( 'edit.php?post_type=shop' ) );
	}

	public function login_redirect( $redirect_to, $request, $user ){
		return admin_url( 'edit.php?post_type=shop' );
	}

	public function clean_up_menu () {
		global $menu;
		$restricted = array( __( 'Dashboard' ) );
		end( $menu );
		while( prev( $menu ) ){
			$value = explode(' ',$menu[key( $menu )][0]);
			if( in_array( $value[0] !=  NULL ? $value[0] : '', $restricted ) ){
				unset($menu[key($menu)]);
			}
		}
	}

	public function customize_toolbar($wp_admin_bar ) {
		if( is_admin() ) {
			$url = str_replace('/manage', '', home_url());
			$sitename = $wp_admin_bar->get_node('site-name');
			$sitename->href = $url;
			$sitename->meta['target'] = '_blank';
			$wp_admin_bar->add_node($sitename);

			$viewsite = $wp_admin_bar->get_node('view-site');
			$viewsite->href = $url;
			$viewsite->meta['target'] = '_blank';
			$wp_admin_bar->add_node($viewsite);

			$wp_admin_bar->remove_node( 'wp-logo' );
		}
	}

	public function current_user_attachments( $query ) {
		$user_id = get_current_user_id();
		if ( $user_id && !current_user_can('edit_other_shops') ) {
			$query['author'] = $user_id;
		}
		return $query;
	}

	public function redirect_front_page_logged_in() {
		if( is_front_page() && is_user_logged_in() ) {
			wp_redirect( admin_url( 'edit.php?post_type=shop' ) );
		}
	}

}
new Admin;

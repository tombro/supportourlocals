<?php
namespace SOL\ShopMap;

class Report
{
	function __construct() {
		add_shortcode( 'report-form', array( $this, 'report_form' ) );
		add_action( 'admin_post_report_shop', array( $this, 'report_shop' ) );
		add_filter( 'manage_shop_posts_columns', array( $this, 'report_column' ) );
		add_action( 'manage_shop_posts_custom_column', array( $this, 'populate_report_column' ), 10, 2);
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ));
	}

	public function report_form() {
		$shop_id 	= filter_input(INPUT_GET, 'shop_id', FILTER_SANITIZE_NUMBER_INT);
		$reported 	= filter_input(INPUT_GET, 'reported', FILTER_SANITIZE_STRING);
		$shop_name 	= get_the_title( $shop_id );
		ob_start();
		if( $reported ):
		?>
		<div class="alert alert-success">
			<?php printf( __( 'Thank you, %s has been reported and will be reviewed shortly.', 'covid-shop-map'), $shop_id ); ?>
		</div>
		<?php elseif( $shop_id && get_post_type( $shop_id ) == 'shop' ): ?>
		<form method="POST" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">
			<div class="form-row">
				<div class="form-group col">
					<label for="name"><?php _e('Name', 'covid-shop-map') ?></label>
					<input type="text" name="name" class="form-control" placeholder="<?php _e('Name', 'covid-shop-map') ?>" required>
				</div>
				<div class="form-group col">
					<label for="email"><?php _e('E-mailaddress', 'covid-shop-map') ?></label>
					<input type="text" name="email" class="form-control" placeholder="<?php _e('E-mail', 'covid-shop-map') ?>" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col">
					<label for="shop"><?php _e('Shop', 'covid-shop-map') ?></label>
					<input type="text" name="shop_name" class="form-control" placeholder="<?php _e('Shop', 'covid-shop-map') ?>" value="<?php echo $shop_name; ?>" disabled required>
					<input type="hidden" name="shop_id" value="<?php echo $shop_id; ?>" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col">
					<label for="reason"><?php _e('Reason', 'covid-shop-map') ?></label>
					<textarea name="reason" cols="30" rows="10" class="form-control" required></textarea>
				</div>
			</div>
			<input type="hidden" name="action" value="report_shop">
			<button type="submit" class="btn btn-primary"><?php _e('Report', 'covid-shop-map'); ?></button>
		</form>
		<?php else: ?>
			<div class="alert alert-warning">
				<?php _e('No shop was specified.', 'covid-shop-map'); ?>
			</div>
		<?php
		endif;
		return ob_get_clean();
	}

	public function report_shop() {
		$name 		= filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
		$email 		= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
		$shop_id	= filter_input(INPUT_POST, 'shop_id', FILTER_SANITIZE_NUMBER_INT);
		$reason		= filter_input(INPUT_POST, 'reason', FILTER_SANITIZE_STRING);
		$redirect	= strtok( $_SERVER['HTTP_REFERER'], '?' );

		if( $name && $email && $shop_id && $reason ) {
			$args = array(
				'post_title' 	=> get_the_title( $shop_id ),
				'post_status' 	=> 'publish',
				'post_type' 	=> 'report'
			);
			$report_id = wp_insert_post( $args );
			update_post_meta( $report_id, 'name', $name );
			update_post_meta( $report_id, 'email', $email );
			update_post_meta( $report_id, 'shop_id', $shop_id );
			update_post_meta( $report_id, 'reason', $reason );

			// Mail
			$to 		 = get_option('admin_email');
			$subject 	 = __( 'Shop reported', 'covid-shop-map' );
			$message 	 = sprintf( '<p>%s,</p>', __( 'Hi', 'covid-shop-map' ) );
			$message 	.= sprintf( '<p>%s has reported "%s"</p>', $name, get_the_title( $shop_id ) );
			$message 	.= sprintf( '<p>Reason:</p>' );
			$message 	.= wpautop( $reason );
			$message 	.= sprintf( '<p><a href="%s">%s</a></p>', get_edit_post_link( $report_id ), __('View report', 'covid-shop-map') );
			$message 	.= sprintf( '<p>%s</p>', __('Stay safe', 'covid-shop-map' ) );

			add_filter( 'wp_mail_content_type', array($this, 'wp_html_mail' ) );
			wp_mail( $to, $subject, $message );
			remove_filter( 'wp_mail_content_type', array($this, 'wp_html_mail' ) );

			$redirect = add_query_arg( array( 'reported' => true, 'shop_id' => $shop_id ), $redirect );
			wp_redirect( $redirect );
		}
	}

	public function report_column( $columns ) {
		unset( $columns['date'] );
		$columns['report'] = __( 'Reports', 'covid-shop-map' );
		$columns['date'] = __( 'Date' );
		return $columns;
	}

	public function populate_report_column( $column, $post_id ) {
		if( $column == 'report' ) {
			$args = array(
				'post_type' => 'report',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key' => 'shop_id',
						'value' => $post_id
					)
				)
			);
			$return = array();
			$reports = get_posts( $args );
			foreach( $reports as $report ) {
				$return[] = sprintf( '<a href="%s"><span class="dashicons dashicons-flag" style="color: #f00;"></span></a>', get_edit_post_link( $report->ID ) );
			}
			echo implode(' ', $return);
		}
	}

	public function add_meta_boxes() {
		add_meta_box( 'report-content', __( 'Report', 'covid-shop-map' ), array( $this, 'report_content' ), 'report' );
	}

	public function report_content() {
		$meta = array(
			'name' => __( 'Name', 'covid-shop-map' ),
			'email' => __( 'E-mailaddress', 'covid-shop-map' ),
			'shop_id' => __( 'Shop', 'covid-shop-map' ),
			'reason' => __( 'Reason', 'covid-shop-map' )
		);
		?>
		<table class="widefat striped">
			<?php foreach ($meta as $key => $label): ?>
				<tr>
					<th><?php echo $label ?></th>
					<?php if( $key == 'shop_id' ): ?>
						<?php $shop_id = get_post_meta( get_the_ID(), $key, true ); ?>
						<td><a href="<?php echo get_edit_post_link( $shop_id ) ?>"><?php echo get_the_title( $shop_id ); ?></a></td>
					<?php else: ?>
						<td><?php echo get_post_meta( get_the_ID(), $key, true ); ?></td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</table>
		<?php
	}

	public function wp_html_mail(){
	    return "text/html";
	}
}
new Report();

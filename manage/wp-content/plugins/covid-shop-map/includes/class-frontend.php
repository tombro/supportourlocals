<?php
namespace SOL\ShopMap;

class Frontend
{
	function __construct(){
		add_filter('acf/format_value/type=wysiwyg', array( $this, 'customize_links' ) , 10, 3);
	}

	public function customize_links( $value, $post_id, $field ) {
		// Remove target
		$value = preg_replace('/(<[^>]+) target=".*?"/i', '$1', $value);
		// Add target _blank
		$value = preg_replace("/<a(.*?)>/", "<a$1 target=\"_blank\">", $value);

		// Remove rel
		$value = preg_replace('/(<[^>]+) rem=".*?"/i', '$1', $value);
		// Add rel noopener noreferer
		$value = preg_replace("/<a(.*?)>/", "<a$1 rel=\"noopener noreferer\">", $value);

		return $value;
	}
}
new Frontend;

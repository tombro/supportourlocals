<?php
namespace SOL\ShopMap;

class Rest_Api
{
	function __construct(){
		add_filter( 'rest_api_init', array( $this, 'add_custom_fields_to_shop' ) );
		add_filter( 'rest_shop_query', array( $this, 'rest_shop_query' ), 10, 2 );
		add_filter( 'rest_shop_collection_params', array( $this, 'show_custom_query_parameters' ), 10, 2);
		add_filter( 'rest_prepare_shop_category', array( $this, 'replace_ampersand_in_taxonomy' ), 10, 3);
		add_filter( 'rest_prepare_shop', array( $this, 'replace_ampersand_in_shop_title' ), 10, 3);
	}

	public function add_custom_fields_to_shop() {
		register_rest_field( 'shop', 'shop_information', array(
			'get_callback'    => array($this, 'custom_fields'),
			'schema'          => null,
		));
		register_rest_field( 'shop', 'report_link', array(
			'get_callback'    => array($this, 'report_link'),
			'schema'          => null,
		));
		register_rest_field( 'shop', 'thumbnail_url', array(
			'get_callback'    => array($this, 'thumbnail_url'),
			'schema'          => null,
		));
	}

	public function custom_fields( $object ) {
		$ID = $object['id'];
		return get_fields($ID);
	}

	public function report_link( $object ) {
		$ID = $object['id'];
		return home_url('report/?shop_id=' . $ID);
	}

	public function thumbnail_url( $object ) {
		$ID = $object['id'];
		return get_the_post_thumbnail_url( $ID, 'thumbnail' );
	}

	public function rest_shop_query( $args, $request ) {
		$filter_meta = $this->custom_query_parameters();
		foreach ($filter_meta as $key => $meta_data) {
			if( isset( $_GET[$key] ) ) {
				$args['meta_key'] = $key;
				$args['meta_value'] = filter_input(INPUT_GET, $key, $meta_data['sanitize']);
			}
		}

		return $args;
	}

	public function show_custom_query_parameters( $query_params, $post_type ) {
		$filter_meta = $this->custom_query_parameters();
		foreach ($filter_meta as $key => $meta_data) {
			$query_params[$key] = $meta_data['info'];
		}
		return $query_params;
	}

	private function custom_query_parameters() {
		return array(
			'postcode' => array(
				'sanitize' => FILTER_SANITIZE_NUMBER_INT,
				'info' => array(
					'description'	=> __( 'Filter by postcode', 'covid-shop-map' ),
					'type'			=> 'integer',
				)
			),
			'city'  => array(
				'sanitize' => FILTER_SANITIZE_STRING,
				'info' => array(
					'description'	=> __( 'Filter by city', 'covid-shop-map' ),
					'type'			=> 'string',
				)
			),
		);
	}

	public function replace_ampersand_in_taxonomy($response, $item, $request) {
		$response->data['name'] = html_entity_decode($response->data['name']);
		return $response;
	}

	public function replace_ampersand_in_shop_title($response, $item, $request) {
		$response->data['title']['rendered'] = html_entity_decode($response->data['title']['rendered']);
		return $response;
	}
}
new Rest_Api;

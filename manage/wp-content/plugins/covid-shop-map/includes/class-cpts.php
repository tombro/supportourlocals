<?php
namespace SOL\ShopMap;

class Cpts
{
	function __construct(){

		add_action( 'init', array( $this, 'generate_cpts' ), 99 );

	}

	public function generate_cpts(){

		$post_types = array(
			array(
				'slug' => 'shop',
				'singular_label' => __('Shop', 'covid-shop-map'),
				'plural_label' => __('Shops', 'covid-shop-map'),
				'options' => array(
					'menu_icon' => 'dashicons-store',
					'supports' => array(
						'title', 'thumbnail', 'author'
					),
					'public' => false,
					'show_in_rest' => true,
					'has_archive' => false,
					'exclude_from_search' => true,
					'publicly_queryable' => false,
				    'capabilities' => array(
				        'edit_post' => 'edit_shop',
				        'edit_posts' => 'edit_shops',
				        'edit_others_posts' => 'edit_others_shops',
				        'publish_posts' => 'publish_shops',
				        'read_post' => 'read_shop',
				        'read_private_posts' => 'read_private_shops',
				        'delete_post' => 'delete_shop',
				        'delete_posts' => 'delete_shops'
				    ),
				),
				'taxonomies' => array(
					'shop_services' => array(
						'labels' => array(
							'singular_label' => __('Service', 'covid-shop-map'),
							'plural_label' => __('Services', 'covid-shop-map'),
						),
						'args' => array(
							'show_in_rest' => true,
							'capabilities' => array(
								'manage_terms' => 'manage_shop_services',
								'edit_terms'   => 'edit_shop_services',
								'delete_terms' => 'delete_shop_services',
								'assign_terms' => 'assign_shop_services',
							)
						)
					),
					'shop_category' => array(
						'labels' => array(
							'singular_label' => __('Category', 'covid-shop-map'),
							'plural_label' => __('Categories', 'covid-shop-map'),
						),
						'args' => array(
							'show_in_rest' => true,
							'capabilities' => array(
								'manage_terms' => 'manage_shop_category',
								'edit_terms'   => 'edit_shop_category',
								'delete_terms' => 'delete_shop_category',
								'assign_terms' => 'assign_shop_category',
							)
						)
					),
				)
			),
			array(
				'slug' => 'report',
				'singular_label' => __('Report', 'covid-shop-map'),
				'plural_label' => __('Reports', 'covid-shop-map'),
				'options' => array(
					'menu_icon' => 'dashicons-shield',
					'supports' => array(
						'title'
					),
					'public' => false,
					'show_in_rest' => false,
					'has_archive' => false,
					'exclude_from_search' => true,
					'publicly_queryable' => false,
				    'capabilities' => array(
				        'edit_post' => 'edit_report',
				        'edit_posts' => 'edit_reports',
				        'edit_others_posts' => 'edit_others_reports',
				        'publish_posts' => 'publish_reports',
				        'read_post' => 'read_report',
				        'read_private_posts' => 'read_private_reports',
				        'delete_post' => 'delete_report',
				        'delete_posts' => 'delete_reports'
				    ),
				)
			)
		);
		foreach ($post_types as $key => $post_type) {
			$this->add_post_type( $post_type );
		}

	}

	private function add_post_type( $args ){

		$singular_name = $args['singular_label'];
		$plural_name = $args['plural_label'];
		$slug = $args['slug'];

		$labels = array(
			'name'               => ucfirst( $plural_name ),
			'singular_name'      => ucfirst( $singular_name ),
			'menu_name'          => ucfirst( $plural_name ),
			'name_admin_bar'     => ucfirst( $singular_name ),
			'add_new'            => __( 'Add new', 'covid-shop-map' ),
			'add_new_item'       => sprintf( __( 'Add new %s', 'covid-shop-map' ), $singular_name ),
			'new_item'           => sprintf( __( 'New %s', 'covid-shop-map' ), $singular_name ),
			'edit_item'          => sprintf( __( 'Edit %s', 'covid-shop-map' ), $singular_name ),
			'view_item'          => sprintf( __( 'View %s', 'covid-shop-map' ), $singular_name ),
			'all_items'          => sprintf( __( 'All %s', 'covid-shop-map' ), $plural_name ),
			'search_items'       => sprintf( __( 'Search %s', 'covid-shop-map' ), $plural_name ),
			'parent_item_colon'  => sprintf( __( 'Parent %s', 'covid-shop-map' ), $singular_name ),
			'parent_item_colon'  => sprintf( __( 'Parent %s:', 'covid-shop-map' ), $singular_name ),
			'not_found'          => sprintf( __( 'No %s found', 'covid-shop-map' ), $plural_name ),
			'not_found_in_trash' => sprintf( __( 'No %s found in trash', 'covid-shop-map' ), $plural_name )
		);

		$defaults = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $slug ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'exclude_from_search'=> true,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
		);

		$post_args = wp_parse_args( $args['options'], $defaults );

		register_post_type( $slug, $post_args );

		if( isset( $args['taxonomies'] ) ){
			foreach ($args['taxonomies'] as $tax_slug => $tax) {
				$this->add_taxonomy( $tax_slug, $tax['labels'], $tax['args'], $slug );
			}
		}

	}

	private function add_taxonomy( $tax_slug, $tax_labels, $tax_args, $post_type) {

		$singular_name = $tax_labels['singular_label'];
		$plural_name = $tax_labels['plural_label'];

		$labels = array(
			'name'              => ucfirst( $plural_name ),
			'singular_name'     => ucfirst( $singular_name ),
			'search_items'      => sprintf( __( 'Search %s', 'covid-shop-map' ), $plural_name ),
			'all_items'         => sprintf( __( 'All %s', 'covid-shop-map' ), $plural_name ),
			'parent_item'       => sprintf( __( 'Parent %s', 'covid-shop-map' ), $singular_name ),
			'parent_item_colon' => sprintf( __( 'Parent %s:', 'covid-shop-map' ), $singular_name ),
			'edit_item'         => sprintf( __( 'Edit %s', 'covid-shop-map' ), $singular_name ),
			'update_item'       => sprintf( __( 'Update %s', 'covid-shop-map' ), $singular_name ),
			'add_new_item'      => sprintf( __( 'Add new %s', 'covid-shop-map' ), $singular_name ),
			'new_item_name'     => sprintf( __( 'New %s name', 'covid-shop-map' ), $singular_name ),
			'menu_name'         => ucfirst( $singular_name ),
		);

		$defaults = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => false,
			'rewrite'           => array( 'slug' => $tax_slug ),
		);

		$tax_args = wp_parse_args( $tax_args, $defaults );
		register_taxonomy( $tax_slug, $post_type, $tax_args );
	}
}
new Cpts;
